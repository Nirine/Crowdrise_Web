<?php

/* CrowdriseProblemesBundle:Default:problems_forum.html.twig */
class __TwigTemplate_8db8676ade9ef571e3d181df3bd40c8b9f0e9ebcb0334e54e52ad3a3d36430a5 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("CrowdriseProblemesBundle::layout.html.twig");

        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'container' => array($this, 'block_container'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "CrowdriseProblemesBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        // line 4
        echo "
<title>Resale a Business Category Flat Bootstrap Responsive Website Template | Categories :: w3layouts</title>


<link rel=\"stylesheet\" href=\"";
        // line 8
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/bootstrap.min.css"), "html", null, true);
        echo "\">
<link rel=\"stylesheet\" href=\"";
        // line 9
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/bootstrap-select.css"), "html", null, true);
        echo "\">
<link href=\"";
        // line 10
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/style.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" type=\"text/css\" media=\"all\" />
<!-- for-mobile-apps -->
<meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">
<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />
<meta name=\"keywords\" content=\"Resale Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, Sony Ericsson, Motorola web design\" />
<script type=\"application/x-javascript\"> addEventListener(\"load\", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<!-- //for-mobile-apps -->
<!--fonts-->
<link href='//fonts.googleapis.com/css?family=Ubuntu+Condensed' rel='stylesheet' type='text/css'>
<link href='//fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
<!--//fonts-->\t
<!-- js -->


<script type=\"text/javascript\" src=\"";
        // line 25
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/jquery.min.js"), "html", null, true);
        echo "\"></script>
<!-- js -->
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src=\"";
        // line 28
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/bootstrap.min.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 29
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/bootstrap-select.js"), "html", null, true);
        echo "\"></script>

<script>
  \$(document).ready(function () {
    var mySelect = \$('#first-disabled2');

    \$('#special').on('click', function () {
      mySelect.find('option:selected').prop('disabled', true);
      mySelect.selectpicker('refresh');
    });

    \$('#special2').on('click', function () {
      mySelect.find('option:disabled').prop('disabled', false);
      mySelect.selectpicker('refresh');
    });

    \$('#basic2').selectpicker({
      liveSearch: true,
      maxOptions: 1
    });
  });
</script>

<script type=\"text/javascript\" src=\"";
        // line 52
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/jquery.leanModal.min.js"), "html", null, true);
        echo "\"></script>
<link href=\"";
        // line 53
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/jquery.uls.css"), "html", null, true);
        echo "\" rel=\"stylesheet\"/>
<link href=\"";
        // line 54
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/jquery.uls.grid.css"), "html", null, true);
        echo "\" rel=\"stylesheet\"/>
<link href=\"";
        // line 55
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/jquery.uls.lcd.css"), "html", null, true);
        echo "\" rel=\"stylesheet\"/>
<!-- Source -->
<script src=\"";
        // line 57
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/jquery.uls.data.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 58
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/jquery.uls.data.utils.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 59
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/jquery.uls.lcd.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 60
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/jquery.uls.languagefilter.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 61
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/jquery.uls.regionfilter.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 62
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/jquery.uls.core.js"), "html", null, true);
        echo "\"></script>


<script>
\t\t\t\$( document ).ready( function() {
\t\t\t\t\$( '.uls-trigger' ).uls( {
\t\t\t\t\tonSelect : function( language ) {
\t\t\t\t\t\tvar languageName = \$.uls.data.getAutonym( language );
\t\t\t\t\t\t\$( '.uls-trigger' ).text( languageName );
\t\t\t\t\t},
\t\t\t\t\tquickList: ['en', 'hi', 'he', 'ml', 'ta', 'fr'] //FIXME
\t\t\t\t} );
\t\t\t} );
\t\t</script>
                
<link rel=\"stylesheet\" type=\"text/css\" href=\"";
        // line 77
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/easy-responsive-tabs.css"), "html", null, true);
        echo "\" />
<script src=\"";
        // line 78
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/easyResponsiveTabs.js"), "html", null, true);
        echo "\"></script>
</head>

";
    }

    // line 83
    public function block_container($context, array $blocks = array())
    {
        // line 84
        echo "    
    \t<!-- Categories -->
\t<!--Vertical Tab-->
\t<div class=\"categories-section main-grid-border\">
\t\t<div class=\"container\">
\t\t\t<h2 class=\"head\">Main Categories</h2>
\t\t\t<div class=\"category-list\">
\t\t\t\t<div id=\"parentVerticalTab\">
\t\t\t\t\t<ul class=\"resp-tabs-list hor_1\">
\t\t\t\t\t\t<li>Mobiles</li>
\t\t\t\t\t\t<li>Electronics & Appliances</li>
\t\t\t\t\t\t<li>Cars</li>
\t\t\t\t\t\t<li>Bikes</li>
\t\t\t\t\t\t<li>Furniture</li>
\t\t\t\t\t\t<li>Pets</li>
\t\t\t\t\t\t<li>Books, Sports & Hobbies</li>
\t\t\t\t\t\t<li>Fashion</li>
\t\t\t\t\t\t<li>Kids</li>
\t\t\t\t\t\t<li>Services</li>
\t\t\t\t\t\t<li>Jobs</li>
\t\t\t\t\t\t<li>Real Estate</li>
\t\t\t\t\t\t<a href=\"all-classifieds.html\">All Ads</a>
\t\t\t\t\t</ul>
\t\t\t\t\t<div class=\"resp-tabs-container hor_1\">
\t\t\t\t\t\t<span class=\"active total\" style=\"display:block;\" data-toggle=\"modal\" data-target=\"#myModal\"><strong>All USA</strong> (Select your city to see local ads)</span>
\t\t\t\t\t\t<div>
\t\t\t\t\t\t\t<div class=\"category\">
\t\t\t\t\t\t\t\t<div class=\"category-img\">
\t\t\t\t\t\t\t\t\t<img src=\"images/cat1.png\" title=\"image\" alt=\"\" />
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t<div class=\"category-info\">
\t\t\t\t\t\t\t\t\t<h4>Mobiles</h4>
\t\t\t\t\t\t\t\t\t<span>5,12,850 Ads</span>
\t\t\t\t\t\t\t\t\t<a href=\"all-classifieds.html\">View all Ads</a>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t<div class=\"clearfix\"></div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"sub-categories\">
\t\t\t\t\t\t\t\t<ul>
\t\t\t\t\t\t\t\t\t<li><a href=\"mobiles.html\">mobile phones</a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"mobiles.html\">Tablets</a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"mobiles.html\">Accessories</a></li>
\t\t\t\t\t\t\t\t\t<div class=\"clearfix\"></div>
\t\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t\t</div>
                                                    
                                                    <a href=\"post-ad.html\">Post your idea</a>
\t\t\t\t\t\t
                                                
                                                </div>
\t\t\t\t\t\t<div>
\t\t\t\t\t\t\t<div class=\"category\">
\t\t\t\t\t\t\t\t<div class=\"category-img\">
\t\t\t\t\t\t\t\t\t<img src=\"images/cat2.png\" title=\"image\" alt=\"\" />
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t<div class=\"category-info\">
\t\t\t\t\t\t\t\t\t<h4>Electronics & Appliances</h4>
\t\t\t\t\t\t\t\t\t<span>2,01,850 Ads</span>
\t\t\t\t\t\t\t\t\t<a href=\"all-classifieds.html\">View all Ads</a>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t<div class=\"clearfix\"></div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"sub-categories\">
\t\t\t\t\t\t\t\t<ul>
\t\t\t\t\t\t\t\t\t<li><a href=\"electronics-appliances.html\">Computers & accessories</a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"electronics-appliances.html\">Tv - video - audio</a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"electronics-appliances.html\">Cameras & accessories</a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"electronics-appliances.html\">Games & Entertainment</a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"electronics-appliances.html\">Fridge - AC - Washing Machine</a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"electronics-appliances.html\">Kitchen & Other Appliances</a></li>
\t\t\t\t\t\t\t\t\t<div class=\"clearfix\"></div>
\t\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div>
\t\t\t\t\t\t\t<div class=\"category\">
\t\t\t\t\t\t\t\t<div class=\"category-img\">
\t\t\t\t\t\t\t\t\t<img src=\"images/cat3.png\" title=\"image\" alt=\"\" />
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t<div class=\"category-info\">
\t\t\t\t\t\t\t\t\t<h4>Cars</h4>
\t\t\t\t\t\t\t\t\t<span>1,98,080 Ads</span>
\t\t\t\t\t\t\t\t\t<a href=\"all-classifieds.html\">View all Ads</a>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t<div class=\"clearfix\"></div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"sub-categories\">
\t\t\t\t\t\t\t\t<ul>
\t\t\t\t\t\t\t\t\t<li><a href=\"cars.html\">Commercial Vehicles</a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"cars.html\">Other Vehicles</a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"cars.html\">Spare Parts</a></li>
\t\t\t\t\t\t\t\t\t<div class=\"clearfix\"></div>
\t\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div>
\t\t\t\t\t\t\t<div class=\"category\">
\t\t\t\t\t\t\t\t<div class=\"category-img\">
\t\t\t\t\t\t\t\t\t<img src=\"images/cat4.png\" title=\"image\" alt=\"\" />
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t<div class=\"category-info\">
\t\t\t\t\t\t\t\t\t<h4>Bikes</h4>
\t\t\t\t\t\t\t\t\t<span>6,17,568 Ads</span>
\t\t\t\t\t\t\t\t\t<a href=\"all-classifieds.html\">View all Ads</a>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t<div class=\"clearfix\"></div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"sub-categories\">
\t\t\t\t\t\t\t\t<ul>
\t\t\t\t\t\t\t\t\t<li><a href=\"bikes.html\">Motorcycles</a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"bikes.html\">Scooters</a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"bikes.html\">Bicycles</a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"bikes.html\">Spare Parts</a></li>
\t\t\t\t\t\t\t\t\t<div class=\"clearfix\"></div>
\t\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div>
\t\t\t\t\t\t\t<div class=\"category\">
\t\t\t\t\t\t\t\t<div class=\"category-img\">
\t\t\t\t\t\t\t\t\t<img src=\"images/cat5.png\" title=\"image\" alt=\"\" />
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t<div class=\"category-info\">
\t\t\t\t\t\t\t\t\t<h4>Furniture</h4>
\t\t\t\t\t\t\t\t\t<span>1,05,168 Ads</span>
\t\t\t\t\t\t\t\t\t<a href=\"all-classifieds.html\">View all Ads</a>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t<div class=\"clearfix\"></div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"sub-categories\">
\t\t\t\t\t\t\t\t<ul>
\t\t\t\t\t\t\t\t\t<li><a href=\"furnitures.html\">Sofa & Dining</a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"furnitures.html\">Beds & Wardrobes</a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"furnitures.html\">Home Decor & Garden</a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"furnitures.html\">Other Household Items</a></li>
\t\t\t\t\t\t\t\t\t<div class=\"clearfix\"></div>
\t\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div>
\t\t\t\t\t\t\t<div class=\"category\">
\t\t\t\t\t\t\t\t<div class=\"category-img\">
\t\t\t\t\t\t\t\t\t<img src=\"images/cat6.png\" title=\"image\" alt=\"\" />
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t<div class=\"category-info\">
\t\t\t\t\t\t\t\t\t<h4>Pets</h4>
\t\t\t\t\t\t\t\t\t<span>1,77,816 Ads</span>
\t\t\t\t\t\t\t\t\t<a href=\"all-classifieds.html\">View all Ads</a>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t<div class=\"clearfix\"></div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"sub-categories\">
\t\t\t\t\t\t\t\t<ul>
\t\t\t\t\t\t\t\t\t<li><a href=\"pets.html\">Dogs</a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"pets.html\">Aquariums</a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"pets.html\">Pet Food & Accessories</a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"pets.html\">Other Pets</a></li>
\t\t\t\t\t\t\t\t\t<div class=\"clearfix\"></div>
\t\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div>
\t\t\t\t\t\t\t<div class=\"category\">
\t\t\t\t\t\t\t\t<div class=\"category-img\">
\t\t\t\t\t\t\t\t\t<img src=\"images/cat7.png\" title=\"image\" alt=\"\" />
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t<div class=\"category-info\">
\t\t\t\t\t\t\t\t\t<h4>Books, Sports & Hobbies</h4>
\t\t\t\t\t\t\t\t\t<span>9,58,458 Ads</span>
\t\t\t\t\t\t\t\t\t<a href=\"all-classifieds.html\">View all Ads</a>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t<div class=\"clearfix\"></div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"sub-categories\">
\t\t\t\t\t\t\t\t<ul>
\t\t\t\t\t\t\t\t\t<li><a href=\"books-sports-hobbies.html\">Books & Magazines</a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"books-sports-hobbies.html\">Musical Instruments</a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"books-sports-hobbies.html\">Sports Equipment</a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"books-sports-hobbies.html\">Gym & Fitness</a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"books-sports-hobbies.html\">Other Hobbies</a></li>
\t\t\t\t\t\t\t\t\t<div class=\"clearfix\"></div>
\t\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div>
\t\t\t\t\t\t\t<div class=\"category\">
\t\t\t\t\t\t\t\t<div class=\"category-img\">
\t\t\t\t\t\t\t\t\t<img src=\"images/cat8.png\" title=\"image\" alt=\"\" />
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t<div class=\"category-info\">
\t\t\t\t\t\t\t\t\t<h4>Fashion</h4>
\t\t\t\t\t\t\t\t\t<span>3,52,345 Ads</span>
\t\t\t\t\t\t\t\t\t<a href=\"all-classifieds.html\">View all Ads</a>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t<div class=\"clearfix\"></div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"sub-categories\">
\t\t\t\t\t\t\t\t<ul>
\t\t\t\t\t\t\t\t\t<li><a href=\"fashion.html\">Clothes</a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"fashion.html\">Footwear</a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"fashion.html\">Accessories</a></li>
\t\t\t\t\t\t\t\t\t<div class=\"clearfix\"></div>
\t\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div>
\t\t\t\t\t\t\t<div class=\"category\">
\t\t\t\t\t\t\t\t<div class=\"category-img\">
\t\t\t\t\t\t\t\t\t<img src=\"images/cat9.png\" title=\"image\" alt=\"\" />
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t<div class=\"category-info\">
\t\t\t\t\t\t\t\t\t<h4>Kids</h4>
\t\t\t\t\t\t\t\t\t<span>8,45,298 Ads</span>
\t\t\t\t\t\t\t\t\t<a href=\"all-classifieds.html\">View all Ads</a>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t<div class=\"clearfix\"></div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"sub-categories\">
\t\t\t\t\t\t\t\t<ul>
\t\t\t\t\t\t\t\t\t<li><a href=\"kids.html\">Furniture And Toys</a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"kids.html\">Prams & Walkers</a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"kids.html\">Accessories</a></li>
\t\t\t\t\t\t\t\t\t<div class=\"clearfix\"></div>
\t\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div>
\t\t\t\t\t\t\t<div class=\"category\">
\t\t\t\t\t\t\t\t<div class=\"category-img\">
\t\t\t\t\t\t\t\t\t<img src=\"images/cat10.png\" title=\"image\" alt=\"\" />
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t<div class=\"category-info\">
\t\t\t\t\t\t\t\t\t<h4>Services</h4>
\t\t\t\t\t\t\t\t\t<span>7,58,867 Ads</span>
\t\t\t\t\t\t\t\t\t<a href=\"all-classifieds.html\">View all Ads</a>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t<div class=\"clearfix\"></div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"sub-categories\">
\t\t\t\t\t\t\t\t<ul>
\t\t\t\t\t\t\t\t\t<li><a href=\"services.html\">Education & Classes</a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"services.html\">Web Development</a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"services.html\">Electronics & Computer Repair</a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"services.html\">Maids & Domestic Help</a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"services.html\">Health & Beauty</a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"services.html\">Movers & Packers</a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"services.html\">Drivers & Taxi</a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"services.html\">Event Services</a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"services.html\">Other Services</a></li>
\t\t\t\t\t\t\t\t\t<div class=\"clearfix\"></div>
\t\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div>
\t\t\t\t\t\t\t<div class=\"category\">
\t\t\t\t\t\t\t\t<div class=\"category-img\">
\t\t\t\t\t\t\t\t\t<img src=\"images/cat11.png\" title=\"image\" alt=\"\" />
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t<div class=\"category-info\">
\t\t\t\t\t\t\t\t\t<h4>Jobs</h4>
\t\t\t\t\t\t\t\t\t<span>5,74,547 Ads</span>
\t\t\t\t\t\t\t\t\t<a href=\"all-classifieds.html\">View all Ads</a>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t<div class=\"clearfix\"></div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"sub-categories\">
\t\t\t\t\t\t\t\t<ul>
\t\t\t\t\t\t\t\t\t<li><a href=\"jobs.html\">Customer Service</a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"jobs.html\">IT</a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"jobs.html\">Online</a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"jobs.html\">Marketing</a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"jobs.html\">Advertising & PR</a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"jobs.html\">Sales</a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"jobs.html\">Clerical & Administration</a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"jobs.html\">Human Resources</a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"jobs.html\">Education</a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"jobs.html\">Hotels & Tourism</a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"jobs.html\">Accounting & Finance</a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"jobs.html\">Manufacturing</a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"jobs.html\">Part - Time</a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"jobs.html\">Other Jobs</a></li>
\t\t\t\t\t\t\t\t\t<div class=\"clearfix\"></div>
\t\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div>
\t\t\t\t\t\t\t<div class=\"category\">
\t\t\t\t\t\t\t\t<div class=\"category-img\">
\t\t\t\t\t\t\t\t\t<img src=\"images/cat12.png\" title=\"image\" alt=\"\" />
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t<div class=\"category-info\">
\t\t\t\t\t\t\t\t\t<h4>Real Estate</h4>
\t\t\t\t\t\t\t\t\t<span>98,156 Ads</span>
\t\t\t\t\t\t\t\t\t<a href=\"all-classifieds.html\">View all Ads</a>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t<div class=\"clearfix\"></div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"sub-categories\">
\t\t\t\t\t\t\t\t<ul>
\t\t\t\t\t\t\t\t\t<li><a href=\"real-estate.html\">Houses</a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"real-estate.html\">Apartments</a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"real-estate.html\">PG & Roommates</a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"real-estate.html\">Land & Plots</a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"real-estate.html\">Shops - Offices - Commercial Space</a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"real-estate.html\">Vacation Rentals - Guest Houses</a></li>
\t\t\t\t\t\t\t\t\t<div class=\"clearfix\"></div>
\t\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"clearfix\"></div>
\t\t\t\t</div>
\t\t\t</div>
\t\t</div>
\t</div>
\t<!--Plug-in Initialisation-->
\t<script type=\"text/javascript\">
    \$(document).ready(function() {

        //Vertical Tab
        \$('#parentVerticalTab').easyResponsiveTabs({
            type: 'vertical', //Types: default, vertical, accordion
            width: 'auto', //auto or any width like 600px
            fit: true, // 100% fit in a container
            closed: 'accordion', // Start closed if in accordion view
            tabidentify: 'hor_1', // The tab groups identifier
            activate: function(event) { // Callback function if tab is switched
                var \$tab = \$(this);
                var \$info = \$('#nested-tabInfo2');
                var \$name = \$('span', \$info);
                \$name.text(\$tab.text());
                \$info.show();
            }
        });
    });
</script>
\t<!-- //Categories -->

";
    }

    public function getTemplateName()
    {
        return "CrowdriseProblemesBundle:Default:problems_forum.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  170 => 84,  167 => 83,  159 => 78,  155 => 77,  137 => 62,  133 => 61,  129 => 60,  125 => 59,  121 => 58,  117 => 57,  112 => 55,  108 => 54,  104 => 53,  100 => 52,  74 => 29,  70 => 28,  64 => 25,  46 => 10,  42 => 9,  38 => 8,  32 => 4,  29 => 3,);
    }
}
