<?php

namespace Crowdrise\ProblemesBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function problemsAction()
    {
        return $this->render('CrowdriseProblemesBundle:Default:problems_forum.html.twig');
    }
}
